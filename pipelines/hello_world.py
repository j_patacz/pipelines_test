class HelloWorld:
    def hello_world(self):
        """This is hello world function docstring with new words"""

        return "Hello World"

    def second_hello_world(self, a=None, b=None):
        """second_hello_world(a=None, b=None) -> None

        Here I continue documentation
        """

        print("Second hello world")
